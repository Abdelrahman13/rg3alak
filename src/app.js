const express = require('express');

const app = express();
const AuthRouter = require('./Routes/AuthRoute');
const UserRouter = require('./Routes/UserRoute');
// const PostRouter = require('./Routes/PostRoute');
const ItemRouter = require('./Routes/ItemRoute');

const ProfileRouter = require('./Routes/ProfileRoute');

require('./DB/mongoose');
require('./Services/ScheduleService');

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    next();
});

app.use('/auth', AuthRouter);
app.use('/users', UserRouter);
// app.use('/posts', PostRouter);
app.use('/posts', ItemRouter);
app.use('/profile', ProfileRouter);
app.use(express.static('public'));

module.exports = app;
