const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const auth = require('../Middleware/auth');

const ItemController = require('../Controllers/ItemController');
const ItemValidator = require('../Validations/ItemValidator');
const ApplyValidatition = require('../Middleware/apply-validation');
const Item = require('../Models/Item');

const router = new express.Router();

router.use(bodyParser.json());

const storage = multer.diskStorage({});
const upload = multer({
    storage,
    limits: {
        fileSize: 1024 * 1024 * 2.5
    },
    fileFilter(req, file, cb) {
        if (file.mimetype.startsWith('image')) {
            cb(null, true);
        } else {
            cb(new Error('Please upload an image'), false);
            cb('Please upload only images.', false);
        }
    }
});

router.post(
    '/',
    auth,
    // ItemValidator.add(),
    // ApplyValidatition,
    ItemController.newPost
);

router.post('/search', auth, ItemController.searchPosts);

router.post(
    '/uploadimg/:postid',
    auth,
    upload.single('postImg'),
    ItemController.fileUpload
);

router.get('/delete/:postid', auth, ItemController.deletePost);

router.put(
    '/:postid',
    auth,
    ItemValidator.objectIdCheck(),
    ApplyValidatition,
    ItemController.updatePost
);

router.get('/all', auth, ItemController.getAll);

router.get('/paginate', auth, ItemController.paginatePosts);

router.get('/:Id', auth, ItemController.getViewItem);

router.get('/', auth, ItemController.getPosts);

module.exports = router;
