const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const auth = require('../Middleware/auth');

const PostController = require('../Controllers/PostController');
const PostValidator = require('../Validations/PostValidator');
const ApplyValidatition = require('../Middleware/apply-validation');

const router = new express.Router();

router.use(bodyParser.json());

const storage = multer.diskStorage({});
const upload = multer({
    storage,
    limits: {
        fileSize: 1024 * 1024 * 2.5
    },
    fileFilter(req, file, cb) {
        if (file.mimetype.startsWith('image')) {
            cb(null, true);
        } else {
            cb(new Error('Please upload an image'), false);
            cb('Please upload only images.', false);
        }
    }
});

router.post(
    '/',
    auth,
    PostValidator.add(),
    ApplyValidatition,
    PostController.newPost
);

router.post('/search', auth, PostController.searchPosts);

router.post(
    '/uploadimg/:postid',
    auth,
    upload.single('postImg'),
    PostController.fileUpload
);

router.delete(
    '/:postid',
    auth,
    PostValidator.objectIdCheck(),
    ApplyValidatition,
    PostController.deletePost
);

router.put(
    '/:postid',
    auth,
    PostValidator.objectIdCheck(),
    ApplyValidatition,
    PostController.updatePost
);

router.get(
    '/:pageno,:sizeperpage',
    auth,
    PostValidator.getPosts(),
    ApplyValidatition,
    PostController.getPosts
);

router.get('/', auth, PostController.getMyPosts);

module.exports = router;
