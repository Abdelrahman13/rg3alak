const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');

const UserController = require('../Controllers/UserController');
const UserValidator = require('../Validations/UserValidators');
const ApplyValidatition = require('../Middleware/apply-validation');
const auth = require('../Middleware/auth');

const router = new express.Router();

router.use(bodyParser.json());

router.post(
    '/register',
    UserValidator.store(),
    ApplyValidatition,
    UserController.store
);

const storage = multer.diskStorage({});
const upload = multer({
    storage,
    limits: {
        fileSize: 1024 * 1024 * 2.5
    },
    fileFilter(req, file, cb) {
        if (file.mimetype.startsWith('image')) {
            cb(null, true);
        } else {
            cb(new Error('Please upload an image'), false);
            cb('Please upload only images.', false);
        }
    }
});

router.post(
    '/avatar',
    auth,
    upload.single('avatar'),
    UserController.uploadAvatar
);

router.get('/getuseravatar', auth, UserController.getMyAvatar);
router.get('/view-user/:userid', auth, UserController.getUser);
router.get('/getavatar/:userid', auth, UserController.getAvatar);
router.post('/mail/:userid', auth, UserController.sendMail);

module.exports = router;
