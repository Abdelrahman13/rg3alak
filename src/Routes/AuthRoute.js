const express = require('express');
const bodyParser = require('body-parser');
const auth = require('../Middleware/auth');
const AuthController = require('../Controllers/AuthController');
const AuthValidator = require('../Validations/AuthValidator');

const router = new express.Router();

router.use(bodyParser.json());

router.post('/login', AuthValidator.login(), AuthController.login);
router.post('/validate', AuthController.validate);

module.exports = router;
