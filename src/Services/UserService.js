const bcrypt = require('bcryptjs');
const User = require('../Models/User');

class UserService {
    async register(firstname, lastname, email, password) {
        let user = await User.findOne({ email });

        if (user) {
            throw new Error(
                'The user is already existed, Please try another Username'
            );
        }
        // To get user gravatar
        const avatar = `http://localhost:3000/uploads/users_avatars/default_avatar.png`;

        // create instance of a User
        user = new User({
            firstname,
            lastname,
            email,
            avatar,
            password
        });

        // Encrypting password
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);
        // Saving user to the database
        await user.save();

        return true;
    }
}

module.exports = UserService;
