const fs = require('fs');

// fileLocation: 0 = userAvatar, 1 = postImage

const filePath = [
    '../rg3alk_front/rag3alak/src/assets/users_avatars',
    '../rg3alk_front/rag3alak/src/assets/post_images'
];

class UploadService {
    deleteOldImage(fileLocation, fileName) {
        // TODO: CHECK BEFORE DELETE IF THE OLD FILE IS EXISTED
        if (fileLocation == 0 || fileLocation == 1) {
            fs.unlink(`${filePath[fileLocation]}/${fileName}.png`, err => {
                if (err) {
                    console.log(`An error occured, Filename: ${fileName}`);
                    return 0;
                }
                console.log(`File ${fileName} deleted!`);
                return 1;
            });
        }
    }

    checkAvatar(fileName) {
        if (fs.existsSync(`${filePath[0]}/${fileName}.png`)) {
            const reslt = `http://localhost:3000/uploads/users_avatars/${fileName}.png`;
            return reslt;
        }
        const reslt = `http://localhost:3000/uploads/users_avatars/default_avatar.png`;
        return reslt;
    }
}

module.exports = UploadService;
