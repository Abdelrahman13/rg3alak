const Post = require('../Models/Post');
const Item = require('../Models/Item');

class PostService {
    async add(user, item, postType) {
        const post = {
            item,
            postType,
            poster: user.id
        };

        if (postType == 'found') {
            post.founder = user.id;
        } else {
            post.owner = user.id;
        }

        await Post.create(post);

        return post;
    }

    async remove(postid) {
        await Post.findByIdAndDelete(postid);
        return true;
    }

    async update(postid, postType, updatedItem) {
        const post = await Post.findByIdAndUpdate(
            postid,
            { postType },
            {
                new: true
            }
        );
        if (updatedItem) {
            const item = await Item.findOneAndUpdate(
                {
                    post: post._id
                },
                { $set: updatedItem },
                { new: true }
            );
            post.item = item;
        }
        await post.save();
        return post;
    }

    async listPosts(pageno, sizeperpage) {
        const posts = await Post.find({})
            .sort({ date: -1 })
            .skip(sizeperpage * (pageno - 1))
            .limit(sizeperpage);
        const count = await Post.count({});
        const totalCount = Math.ceil(count / sizeperpage);
        posts.totalPagesCount = totalCount;
        return posts;
    }

    async listMyPosts(user) {
        const posts = await Post.find({ poster: user.id });
        return posts;
    }

    async searchPosts(category, coordinates) {
        // coordinates = [longtitude, latitude]
        let sQuery = ``;
        if (category && coordinates) {
            sQuery = `{
                category: ${category},
                location: {
                    $near: {
                        $minDistance: 5,
                        $maxDistance: 1000,
                        $geometry: {
                            type: 'Point',
                            coordinates: ${coordinates}
                        }
                    }
                }
            }`;
        } else if (category && !coordinates) {
            sQuery = `{category: ${category}}`;
        } else if (!category && coordinates) {
            sQuery = `{
                location: {
                $near: {
                    $minDistance: 5,
                    $maxDistance: 1000,
                    $geometry: {
                        type: 'Point',
                        coordinates: ${coordinates}
                    }
                }
            }
        }`;
        }
        const posts = await Item.find(sQuery);
        return posts;
    }
}

module.exports = PostService;
