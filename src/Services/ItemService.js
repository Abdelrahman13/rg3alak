const Item = require('../Models/Item');

class PostService {
    async add(user, item) {
        // const post = {
        //     category: item.category,
        //     title: item.title,
        //     postType: item.postType,
        //     details: item.details,
        //     decription: item.decription,
        //     location,
        //     addressLine: item.addressLine,
        //     poster: user.id
        // };
        item.poster = user.id;
        if (item.postType == 'found') {
            item.founder = user.id;
        } else {
            item.owner = user.id;
        }
        const newItem = await Item.create(item);
        const { coordinates } = item.location;
        const similarItems = await Item.find({
            location: {
                $near: {
                    $maxDistance: 1000,
                    $geometry: {
                        type: 'Point',
                        coordinates
                    }
                }
            }
        });

        return { newItemId: newItem._id, similarItems };
    }

    async remove(postid) {
        await Item.findByIdAndDelete(postid);
        return true;
    }

    async update(postid, postType, updatedItem) {
        const item = await Item.findOneAndUpdate(
            {
                _id: postid
            },
            { $set: updatedItem, postType },
            { new: true }
        );
        await item.save();
        return item;
        /* const post = await Item.findByIdAndUpdate(
            postid,
            { postType },
            {
                new: true
            }
        ); 
        if (updatedItem) {
            const item = await Item.findOneAndUpdate(
                {
                    _id: postid
                },
                { $set: updatedItem, postType },
                { new: true }
            );
            post.item = item;
        } 
        await post.save();
        return post;
        */
    }

    async listPosts(pageno, sizeperpage) {
        const posts = await Item.find({})
            .sort({ date: -1 })
            .skip(sizeperpage * (pageno - 1))
            .limit(sizeperpage);
        const count = await Item.count({});
        const totalCount = Math.ceil(count / sizeperpage);
        posts.totalPagesCount = totalCount;
        return posts;
    }

    async getAllMyPosts(id) {
        const posts = await Item.find({ poster: id });
        return posts;
    }

    async listMyPosts(user) {
        const posts = await Item.find({ poster: user.id });
        return posts;
    }

    async getViewItem(Id) {
        const item = await Item.findById(Id).populate({
            path: 'poster',
            select: {
                password: 0
            },
            model: 'User',
            populate: {
                path: 'profile',
                model: 'Profile'
            }
        });
        return item;
    }

    async searchPosts(category, coordinates) {
        let posts;
        if (category && !coordinates) {
            posts = await Item.find({ category });
        } else if (coordinates && !category) {
            posts = await Item.find({
                location: {
                    $near: {
                        $maxDistance: 1000,
                        $geometry: {
                            type: 'Point',
                            coordinates
                        }
                    }
                }
            });
        }
        /* else {
            Item.find({
                category,
                location: {
                    $near: {
                        $maxDistance: 1000,
                        $geometry: {
                            type: 'Point',
                            coordinates
                        }
                    }
                }
            });
        } */
        return posts;
    }

    async getAllItems() {
        const posts = await Item.find({});
        return posts;
    }
}

module.exports = PostService;
