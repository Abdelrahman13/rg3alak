const bcrypt = require('bcryptjs');
const User = require('../Models/User');

class AuthService {
    async login(email, password) {
        const user = await User.findOne({ email });

        if (!user || !user.isValidated) {
            return false;
        }

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) return false;
        const payload = {
            user: {
                id: user.id,
                firstname: user.firstname,
                lastname: user.lastname,
                role: user.role
            }
        };
        return payload;
    }

    async verify(email, code) {
        const validatedUser = await User.findOne({ email });

        if (validatedUser.validationCode == code) {
            await validatedUser.update({ isValidated: true });
            // Creating token & send it
            const payload = {
                user: {
                    id: validatedUser.id,
                    firstname: validatedUser.firstname,
                    lastname: validatedUser.lastname,
                    role: validatedUser.role
                }
            };
            return payload;
        }
        return false;
    }
}
module.exports = AuthService;
