const S = require('string');
const Profile = require('../Models/Profile');
const User = require('../Models/User');

class ProfileService {
    async newProfile(user, profile) {
        profile.user = user;
        const newProfile = await Profile.findOneAndUpdate(
            { user },
            profile.profile,
            {
                new: true,
                upsert: true
            }
        );
        const userUpdates = { profile: newProfile._id };
        if (profile.firstname) userUpdates.firstname = profile.firstname;
        if (profile.lastname) userUpdates.lastname = profile.lastname;

        await User.findByIdAndUpdate(user, {
            $set: userUpdates
        });
        return newProfile;
    }

    async getProfile(Id) {
        const profile = await User.findById(Id)
            .select({
                password: 0,
                email: 0,
                validationCode: 0,
                _id: 0,
                isValidated: 0,
                avatar: 0
            })
            .populate({
                path: 'profile',
                model: 'Profile',
                select: { _id: 0, __v: 0, user: 0 }
            })
            .exec();
        return profile;
    }

    async deleteProfile(Id, user) {
        const x = await Profile.findOneAndDelete({ _id: Id, user });
        return x;
    }
}

module.exports = ProfileService;
