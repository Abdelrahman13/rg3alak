const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    avatar: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    },
    contactDetails: {
        facebook: String,
        mail: String,
        whatsapp: String,
        phoneNumber: String
    },
    isValidated: {
        type: Boolean,
        default: false
    },
    validationCode: {
        type: Number,
        default: Math.floor(1000 + Math.random() * 9000)
    },
    profile: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Profile'
    },
    role: {
        type: String,
        enum: ['user', 'admin'],
        default: 'user'
    }
});

module.exports = UserSchema;
