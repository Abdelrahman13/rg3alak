const mongoose = require('mongoose');

const ItemSchema = new mongoose.Schema({
    poster: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    founder: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    owner: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    date: {
        type: Date,
        default: Date.now
    },
    postType: {
        type: String,
        enum: ['lost', 'found']
    },
    decription: {
        type: String
    },
    title: {
        type: String
    },
    category: {
        type: String
    },
    addressLine: {
        type: String
    },
    color: String,
    size: String,
    serial: String,
    specialNote: String,
    nationalId: String,
    brand: String,
    location: {
        type: {
            type: 'String',
            default: 'Point'
        },
        coordinates: {
            type: [Number],
            index: '2dsphere'
        }
    }
});

ItemSchema.index({ location: '2dsphere' });
module.exports = ItemSchema;
