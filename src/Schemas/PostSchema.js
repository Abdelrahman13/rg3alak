const mongoose = require('mongoose');
// const Item = require('../Models/Item');

const PostSchema = new mongoose.Schema({
    poster: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    founder: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    owner: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    item: Object,
    date: {
        type: Date,
        default: Date.now
    },
    postType: {
        type: String,
        enum: ['lost', 'found']
    },
    img: {
        type: String
    }
});

module.exports = PostSchema;
