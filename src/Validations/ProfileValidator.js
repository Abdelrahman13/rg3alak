const { check } = require('express-validator');
const { Types } = require('mongoose');

const update = () => [
    check('profile', 'info is required').exists(),
    check('Id')
        .exists()
        .custom(value => {
            if (!Types.ObjectId.isValid(value)) {
                throw new Error('Not an ID');
            }
            return true;
        })
];
const objectIdCheck = () => [
    check('Id')
        .exists()
        .custom(value => {
            if (!Types.ObjectId.isValid(value)) {
                throw new Error('Not an ID');
            }
            return true;
        })
];
module.exports = { update, objectIdCheck };
