const { check } = require('express-validator');

const login = () => [
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Password is required').exists()
];
module.exports = { login };
