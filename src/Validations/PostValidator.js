const { check, body } = require('express-validator');
const { Types } = require('mongoose');

// add or create?
const add = () => [
    check('item', 'An item is required to make a post').exists(),
    body('item.location', "You've to insert a location").exists(),
    body('item.category', "You've to select a category").exists(),
    body('item.decription', "You've to fill the description").exists(),
    body('item.details', "You've to fill at least one of the details").exists(),
    check('postType', 'postType is required to be created').exists()
];

const objectIdCheck = () => [
    check('postid')
        .exists()
        .custom(value => {
            if (!Types.ObjectId.isValid(value)) {
                throw new Error('Not an ID');
            }
            return true;
        })
];

const getPosts = () => [
    check('pageno', 'Page number is required')
        .exists()
        .isInt({ min: 1 }),
    check('sizeperpage', 'Posts count per page is required')
        .exists()
        .isInt({ min: 1 })
];

module.exports = { add, objectIdCheck, getPosts };
