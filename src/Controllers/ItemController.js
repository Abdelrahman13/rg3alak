/* eslint-disable radix */
const status = require('http-status-codes');
const sharp = require('sharp');
const ItemService = require('../Services/ItemService');
const UploadService = require('../Services/UploadService');
const Item = require('../Models/Item');
const { post } = require('../Schemas/ItemSchema');

const itemService = new ItemService();
const uploadService = new UploadService();

const newPost = async (req, res) => {
    try {
        console.log(req.body);
        const { user } = req;
        const { item } = req.body;
        const result = await itemService.add(user, item);
        return res.status(status.OK).send(result);
    } catch (e) {
        return res.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

const fileUpload = async (req, res) => {
    const { postid } = req.params;
    // uploadService.deleteOldImage(1, postid);
    sharp(req.file.path).toFile(
        `D:/Projects/rag3alk/rg3alak/public/uploads/post_images/${postid}.png`,
        err => {
            if (err) {
                console.log(err);
                res.status(status.INTERNAL_SERVER_ERROR).send();
            }
        }
    );
    setTimeout(() => {
        console.log('Done');
        return res.status(status.OK).json('Post Image has been uploaded');
    }, 1);
};

const deletePost = async (req, res) => {
    const { postid } = req.params;
    const confirmation = await itemService.remove(postid);
    if (confirmation) {
        res.status(status.OK).send(true);
    } else {
        res.status(status.NOT_FOUND).json({
            msg: "Couldn't find that post"
        });
    }
};

const updatePost = async (req, res) => {
    const { postid } = req.params;
    const { postType, item } = req.body;
    const posts = await itemService.update(postid, postType, item);
    if (post) {
        res.status(status.OK).send(posts);
    } else {
        res.status(status.NOT_FOUND).json({
            msg: "Couldn't find that post"
        });
    }
};

const updateItem = async (req, res) => {
    const { itemid } = req.params;
    const { updateObj } = req.body;
    const item = await itemService.updateItem(itemid, updateObj);
    if (item) {
        res.status(status.OK).send(item);
    } else {
        res.status(status.NOT_FOUND).json({
            msg: "Couldn't find that item"
        });
    }
};

const getPosts = async (req, res) => {
    const posts = await itemService.getAllMyPosts(req.user.id);
    if (posts) {
        res.status(status.OK).send(posts);
    } else {
        res.status(status.NOT_FOUND).json({
            msg: 'No posts found!'
        });
    }
};

const paginatePosts = async (req, res) => {
    try {
        // Starting of Pagenation
        let pageNo = parseInt(req.query.pageNo);
        const size = parseInt(req.query.size);
        const query = {};
        let response;
        if (!pageNo || pageNo < 0 || pageNo === 0) {
            pageNo = 1;
        }

        // The logic of pagination
        query.skip = size * (pageNo - 1);
        query.limit = size;
        query.sort = { date: -1 };
        // Count  documents
        await Item.count({ company: req.user.company }, async function(
            err,
            totalCount
        ) {
            if (err) {
                response = { error: true, message: 'Error fetching data' };
            }
            await Item.find(
                {},
                {},
                query,
                // eslint-disable-next-line no-shadow
                (err, docs) => {
                    if (err) {
                        response = {
                            error: true,
                            message: 'Error fetching data'
                        };
                        return res.status(400).json(response);
                    }
                    if (!err) {
                        const totalPages = Math.ceil(totalCount / size);
                        response = {
                            error: false,
                            message: docs,
                            pages: totalPages,
                            totalRecords: totalCount
                        };
                        return res.status(200).json(response);
                    }
                }
            );
        });
    } catch (err) {
        console.error(err);
        return res.status(500).send('Server Error');
    }
};

const getMyPosts = async (req, res) => {
    const { user } = req;
    const posts = await itemService.listMyPosts(user);
    if (posts) {
        res.status(status.OK).send(posts);
    } else {
        res.status(status.NOT_FOUND).json({
            msg: 'No posts found!'
        });
    }
};

const getViewItem = async (req, res) => {
    const item = await itemService.getViewItem(req.params.Id);
    if (item) {
        res.status(status.OK).send(item);
    } else {
        res.status(status.NOT_FOUND).json({
            msg: 'No Item Found!'
        });
    }
};

const searchPosts = async (req, res) => {
    const { category } = req.body;
    const { coordinates } = req.body;
    if (category || coordinates) {
        const posts = await itemService.searchPosts(category, coordinates);
        if (posts) {
            res.status(status.OK).send(posts);
        } else {
            res.status(status.NO_CONTENT).send();
        }
    } else {
        res.status(status.NOT_FOUND).send();
    }
};

const getAll = async (req, res) => {
    const posts = await itemService.getAllItems();
    return res.status(status.OK).send(posts);
};

module.exports = {
    newPost,
    deletePost,
    updatePost,
    updateItem,
    getPosts,
    getMyPosts,
    paginatePosts,
    fileUpload,
    searchPosts,
    getViewItem,
    getAll
};
