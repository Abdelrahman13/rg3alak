const status = require('http-status-codes');
const { validationResult } = require('express-validator');
const sharp = require('sharp');

const UserService = require('../Services/UserService');
const UploadService = require('../Services/UploadService');
const User = require('../Models/User');
const Mail = require('../Services/MailerService');

const userService = new UserService();
const uploadService = new UploadService();
const mailService = new Mail();

// eslint-disable-next-line consistent-return
const store = async (req, res) => {
    try {
        const { firstname, lastname, email, password } = req.body;
        const created = await userService.register(
            firstname,
            lastname,
            email,
            password
        );

        if (created) return res.status(status.OK).send(created);
    } catch (e) {
        return res.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

const uploadAvatar = async (req, res) => {
    uploadService.deleteOldImage(0, req.user.id);
    sharp(req.file.path).toFile(
        `D:/Projects/rag3alk/rg3alak/public/uploads/users_avatars/${req.user.id}.png`,
        err => {
            if (err) {
                console.log(err);
                res.status(status.INTERNAL_SERVER_ERROR).send();
            }
        }
    );
    await User.findById(req.user.id).update({
        avatar: `http://localhost:3000/uploads/users_avatars/${req.user.id}.png`
    });
    setTimeout(() => {
        return res
            .status(status.OK)
            .json(
                `http://localhost:3000/uploads/users_avatars/${req.user.id}.png`
            );
    }, 3);
};

const getMyAvatar = async (req, res) => {
    const result = await User.findById(req.user.id, 'avatar -_id').exec();

    res.status(status.OK).json({
        avatar: result.avatar,
        firstname: req.user.firstname,
        lastname: req.user.lastname
    });
};

const getAvatar = async (req, res) => {
    const { userid } = req.params;
    const result = uploadService.checkAvatar(userid);
    return res.status(status.OK).json({ avatar: result });
};

const sendMail = async (req, res) => {
    try {
        const { userid } = req.params;
        const { text } = req.body;
        await mailService.send(userid, text, req.user.id);
        return res.status(status.OK).send(true);
    } catch (e) {
        return res.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

const getUser = async (req, res) => {
    try {
        const { userid } = req.params;
        const user = await User.findById(userid)
            .select({ _id: 0, password: 0 })
            .populate({ path: 'profile', model: 'Profile' });
        return res.status(status.OK).send(user);
    } catch (e) {
        return res.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

module.exports = {
    store,
    uploadAvatar,
    getMyAvatar,
    getAvatar,
    sendMail,
    getUser
};
