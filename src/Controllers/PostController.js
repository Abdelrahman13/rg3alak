const status = require('http-status-codes');
const sharp = require('sharp');
const PostService = require('../Services/PostService');
const UploadService = require('../Services/UploadService');

const postService = new PostService();
const uploadService = new UploadService();

const newPost = async (req, res) => {
    // console.log(req.body.item);
    // console.log(req.body.item.location.addressLine);
    const { user } = req;
    const { postType, item } = req.body;
    const post = await postService.add(user, item, postType);
    res.status(status.OK).send(post);
};

const fileUpload = async (req, res) => {
    const { postid } = req.params;
    uploadService.deleteOldImage(1, postid);
    sharp(req.file.path).toFile(
        `../rg3alk_front/rag3alak/src/assets/post_images/${postid}.png`,
        err => {
            if (err) {
                console.log(err);
                res.status(status.INTERNAL_SERVER_ERROR).send();
            }
        }
    );
    res.status(status.OK).send('Post Image has been uploaded');
};

const deletePost = async (req, res) => {
    const { postid } = req.params;
    const confirmation = await postService.remove(postid);
    if (confirmation) {
        res.status(status.OK).send();
    } else {
        res.status(status.NOT_FOUND).json({
            msg: "Couldn't find that post"
        });
    }
};

const updatePost = async (req, res) => {
    const { postid } = req.params;
    const { postType, item } = req.body;
    const post = await postService.update(postid, postType, item);
    if (post) {
        res.status(status.OK).send(post);
    } else {
        res.status(status.NOT_FOUND).json({
            msg: "Couldn't find that post"
        });
    }
};

const updateItem = async (req, res) => {
    const { itemid } = req.params;
    const { updateObj } = req.body;
    const item = await postService.updateItem(itemid, updateObj);
    if (item) {
        res.status(status.OK).send(item);
    } else {
        res.status(status.NOT_FOUND).json({
            msg: "Couldn't find that item"
        });
    }
};

const getPosts = async (req, res) => {
    console.log('Hi')
    const { pageno, sizeperpage } = req.params;
    const pagenoC = Number(pageno);
    const sizeperpageC = Number(sizeperpage);
    const posts = await postService.listPosts(pagenoC, sizeperpageC);
    if (posts) {
        // res.status(status.OK).send(posts);
        res.status(status.OK).json({
            data: posts,
            totalcount: posts.totalPagesCount
        });
    } else {
        res.status(status.NOT_FOUND).json({
            msg: 'No posts found!'
        });
    }
};

const getMyPosts = async (req, res) => {
    const { user } = req;
    const posts = await postService.listMyPosts(user);
    if (posts) {
        res.status(status.OK).send(posts);
    } else {
        res.status(status.NOT_FOUND).json({
            msg: 'No posts found!'
        });
    }
};

const searchPosts = async (req, res) => {
    const { category } = req;
    const { coordinates } = req;
    if (category || coordinates) {
        const posts = await postService.searchPosts(category, coordinates);
        if (posts) {
            res.status(status.OK).send(posts);
        } else {
            res.status(status.NO_CONTENT).send();
        }
    } else {
        res.status(status.NOT_FOUND).send();
    }
};

module.exports = {
    newPost,
    deletePost,
    updatePost,
    updateItem,
    getPosts,
    getMyPosts,
    fileUpload,
    searchPosts
};
